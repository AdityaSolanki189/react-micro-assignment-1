import React from 'react';
import Form from './Form';
import vector from '../assets/vector.png';
import './Header.css';

function Header() {
  return (
    <div className="header-container">
        <div className='header'>
            <div className="header-left">
                <h1>An inspiring design delivered to your inbox every morning</h1>
                <p id="mid-text">Our team scouts the internet for the best designs, illustrations and art and delivers a truly inspiring one every day to your inbox </p>
                <h3>Show me how it looks</h3>
                <Form/>
                <p id="text" >Free - No Spam - No Data Sharing</p>
            </div>
            <div className="header-right">
                <div className="image-container">
                    <img src={vector} alt="person"/>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Header;