import React from 'react'
import './Form.css';

function Form() {
  return (
    <div>
        <form action="/" method="POST" className="register-container">
            <input type="email" placeholder="Your e-mail address" required/>
            <button type="submit" name="submit">Register Now</button>
        </form>
    </div>
  )
}

export default Form