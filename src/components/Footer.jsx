import React from 'react'
import logo1 from '../assets/LinkedinLogo.png';
import logo2 from '../assets/InstagramLogo.png';
import './Footer.css';

function Footer() {
  return (
    <div className="footer-container">
        <div className="footer">
            <div className="footer-left">
                <p>Prompt Generator</p>
                <p>Dweep Daily</p>
                <p>All Contributions</p>
                <p>Your Data on Dweep.io</p>
                <p>Contribute to Dweep</p>
            </div>
            <div className="footer-right">
                <p>Dweep.ip</p>
                <p>Made with love in India</p>

                <div className="social-media">
                    <img src={logo1} alt="LinkedIn icon"/>
                    <img src={logo2} alt="instagram icon"/>
                </div>
                <p>hello@dweep.io</p>
            </div>
        </div>
    </div>
  )
}

export default Footer;